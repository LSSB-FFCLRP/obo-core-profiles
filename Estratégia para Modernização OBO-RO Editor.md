# Estratégia para Modernização OBO-RO Editor



{:toc}



## Opção 1: Manter metamodelo ODM, criar perfis ODM e RO

- Estágios:
  - F: Arquivos OBOFFF
  - M1: Modelo ODM
  - M2: Modelo UML anotado com perfis
- Transformações
  - Arquivo OBOFFF <-> Modelo ODM: Já implementada (OBO-Edit API + código)
  - Modelo ODM <-> Modelo UML: Refatoração das transformações implementadas (ATL)

### Pros

- Não reimplementa injeção e extração;
- Nãofaz grandes mudanças na arquitetura geral;

### Cons

- Não resolve a crítica da escolha do OBOFFF;
- Transformação entre M1 e M2 é complexa e hoje sofre da dificuldade gerada pela diferença entre esses domínios (quando M2 é o OBO-RO);
  - Porém, a separação entre conceitos UML e detalhes do ODM (que serão tratados pelo perfil ODM) pode reduzir essa complexidade (putative PRO); 



## Opção 2: Tirar metamodelo ODM, incluir metamodelo OWL, usar perfil OWL e criar perfil RO

- Estágios:
  - F: Arquivos OWL
  - M1: Modelo OWL
  - M2:  Modelo UML anotado com perfis
- Transformações
  - Arquivos OWL <-> modelo OWL:  OWL API ( ou Protegé OWL API) + código
  - Modelo OWL <-> Modelo UML: ATL

### Pros

- Resolve a crítica da escolha da linguagem e moderniza o editor, que poderá utilizar ontologias OWL por meio de API já utilizada pelo Protegé;
- Apoia-se em um perfil provido pela OMG para a representação de conceitos OWL (usável no papyrus);
- Encontrei metamodelos que se propõe a representar OWL
  - Porém, pode ser preciso algum ajuste nesses metamodelos de modo a torná-los semelhantes ao pacote `model` da OWL API (putative CON);
    - Ajustes podem ser feitos durante a criação da transformação F->M1 
- Reduz o *impedance mismatching* das transformações entre  M1 e M2, o que deve tornar o mapeamento entre esses modelos mais direto e de manutenção mais fácil;

### Cons

- Precisa implementar novamente injeção e extração (+1 semana);
- Precisa implementar novamente as transformações entre M1 e M2;
  - Suporte do Annex D - Mapping UMLto OWL do OWL 1.1 (putative PRO)
    - Anexo também apresenta pontos de dificuldade no processo de mapear esses domínios, bem como limites do mapeamento (conceitos só existentes em um dos domínios)
  - Na primeira vez eu tentei fazer reuso das regras ao máximo. Isso complicou bastante a implementação. Agora, penso em fazer regras apenas para as metaclasses concretas do modelo, o que deve ser mais simples (PRO). 
- ​
## Estratégia Opção 1 (6 semanas):

### 1. Definir perfil OBOFFF / ODM (2 dias)
### 2. Definir perfil RO (1-1.5 semanas)
### 3. Redefinir transformações, agora usando metamodelo UML e perfis (1 semana)
### 4. Usar o Papyrus como editor gráfico e extendê-lo para adicionar ferramentas próprias (2-3 semanas)
### 5. Atualizar código de suporte (menus e automações) (1 semana?)

## Estratégia Opção 2 (8 semanas):

### 1. Criar um perfil UML em XMI para a nova RO (~1 semana)

- ~~[Perfil UML para OWL](http://www.omg.org/spec/ODM/20080901/OWL.profile.uml) Uśavel no Papyrus~~

- ~~[Perfils UML do OMG-ODM](http://www.omg.org/spec/ODM/20080901/profiles.xmi) Gera algum erro durante carregamento no Papyrus~~


- [Perfil OWL do ODM 1.1 em XMI [Normativo]](http://www.omg.org/spec/ODM/20131101/OWLProfile.xmi)

- [Perfil RDF do ODM 1.1 em XMI [Normativo]](http://www.omg.org/spec/ODM/20131101/RDFProfile.xmi) 

  - [Importação de  XMI da OMG](https://www.eclipse.org/forums/index.php/t/798393/):

    > 1. ​
    >
    > 2. ​
    >
    > 3. Replace the base element <xmi:XMI/> with <uml:Model/> from a blank papyrus model.
    >
    > 4. Delete the trailing <mofext:xyz/> tag which followed the package.
    >
    > 5. Change the <uml:Package/> item to <packagedElement type="uml:Package"/> ...
    >
    > 6. Change four upper bounds from 0 to 1.
    >
    > 7. Change a <type href="OMG URL reference to type"/> to href="[platform://blahblah](platform://blahblah)" when referring to a String.
    >
    > After that, I can do a package import and use the OMG-defined model from my own models. However, I am noticing that the import was imperfect. Whatever wrote the OMG file appears to represent a multiplicity of "*" by omitting the lower bound and specifying an upper bound of "*". Papyrus/eclipse interprets this as "1..*". 


- [BFO](http://obofoundry.org/ontology/bfo.html) define apenas CLASSES DE ENTIDADES
- Definir apenas (ou primeiro) para [RO Core](https://github.com/oborel/obo-relations/wiki/ROCore)? 
  - [ROCore.owl](https://raw.githubusercontent.com/oborel/obo-relations/master/core.owl) 
  - Apenas  relações independentes de domínio
    - 15 classes de entidades, 
    - 28 object properties
      - '2D boundary of'
      - 'bearer of'
        - 'has disposition'
        - 'has function'
        - 'has quality'
        - 'has role'
      - concretizes
      - 'contains process'
      - 'derives from'
      - 'derives into'
      - 'has 2D boundary'
      - 'has part'
        - 'has member'
      - 'has participant'
      - 'inheres in'
        - 'disposition of'
        - 'function of'
        - 'quality of'
        - 'role of'
      - 'is concretized as'
      - 'located in'
      - 'location of'
      - 'occurs in'
      - 'part of'
        - 'member of'
      - 'participates in'
      - 'realized in'
      - realizes
- RO Completa possui [76 classes e 505 Object Properties](http://www.ontobee.org/ontology/RO)
- [Validação de restrições do Papyrus em perfis](https://help.eclipse.org/neon/index.jsp?nav=%2F66_1)




#### Questões

- Como ter estereótipos que representam classes e tipos de relacionamentos definidos em uma ontologia A e, ao mesmo tempo, utilizar esses estereótipos de classes UML que representam termos de uma ontologia B, dado que A e B são originalmente definidos em um mesmo nível (i.e., ambos são definidos originalmente como modelos do metamodelo ODM/OBOFFF)?

### 2. Criar injetor e extrator arquivo OWL <-> modelo OWL (~1.5 semana)
- [OWL API](https://github.com/owlcs/owlapi): API de serialização e representação em memória
- [Metamodelos do ODM 1.1 em XMI [Normativo]](http://www.omg.org/spec/ODM/20131101/ODM-metamodels.xmi)
- - [EMF 4 Semantic Web](https://github.com/ghillairet/emf4sw/) Tem um metamodelo Ecore que foi criado  antes do projeto  ser deixado de lado
- [Metamodelo Ecore para OWL provido pela W3C](https://code.google.com/archive/p/owl2/downloads)

### 3. Criar transformações Modelo OWL <-> UML+Perfil (2 semanas)
- Suporte do Anexo D  do OMG-ODM

### 4. Usar o Papyrus como editor gráfico e extendê-lo para adicionar ferramentas próprias (2-3 semanas)

- [Personalização Papyrus for Information Modeling](https://wiki.eclipse.org/Papyrus_for_Information_Modeling/Customization_Guide#Papyrus_Customization) Provavelmente, o melhor exemplo para a atividade

  > In order to support Information Modeling as a specific domain, we therefore adapt different aspects of Papyrus. Most of these adaptations are restrictions on the original functionality of Papyrus, as the core idea of Information Modeling is the focus on a **specific subset of UML**.


- [**Papyrus TSG para viewpoints**](https://help.eclipse.org/neon/index.jsp?nav=%2F66_1): Parece apresentar o que precisamos para essa definição
- [Papyrus Developer Guide](https://wiki.eclipse.org/Papyrus_Developer_Guide#Papyrus_Architecture)
- [Papyrus Toolsmith Guide](https://help.eclipse.org/neon/index.jsp?nav=%2F66_1)
- [Papyrus TSG para Facets](https://help.eclipse.org/neon/index.jsp?nav=%2F66_1): Pode ser usado para definir os grupos diferentes de elementos de um modelo
- [Papyrus TSG para ElementTypesConfigurations](https://help.eclipse.org/neon/index.jsp?topic=%2Forg.eclipse.papyrus.infra.types.doc%2Ftarget%2Fgenerated-eclipse-help%2Ftypes.html&cp=66_1_4): Registro que permite incliir novos construtos específicos de aplicação que seram requeridos em comandos de alto nível.
- [Contribuindo com a paleta](http://timezra.blogspot.com.br/2014/04/contributing-to-papyrus-editor-palette.html)


- [Fórum - Customizando palleta com viewpoints](https://www.eclipse.org/forums/index.php/t/1086137/) 
> Now, I want customize the component and sequence diagram palettes in order to add the stereotyped classes to the palettes to make working with the profile easier.
>
> I saw that extending "org.eclipse.papyrus.uml.diagram.common.paletteDefinition" extension point with a *.xml file is deprecated. And if i do this I would need to projects due to it only applies the last extension.
>
> Then I am trying to customize the palettes with Viewpoints but I think i am doing something wrong because I have some problems.
>
> 1. I am trying to customize the palette configuring the "Custom Palette" property of the diagram with a *.xml file. I am not sure if a can put a .xml file there or I need to put a .paletteConfiguration file, but I don't know how to create it. I think I have to do it writting the file directly but I don't know where I can read documentation about it.
>   I got the xml file customizing the palette with papyrus and searching the file in the papyrus folder.
>
> 2. I saw in "[https://help.eclipse.org/neon/topic/org.eclipse.papyrus.infra.viewpoints.doc/target/generated-eclipse-help/viewpoints.html?cp=66_1_0_3_1#Walkthrough](https://help.eclipse.org/neon/topic/org.eclipse.papyrus.infra.viewpoints.doc/target/generated-eclipse-help/viewpoints.html?cp=66_1_0_3_1#Walkthrough)" that giving an URI of the form platform:/plugin/my.plugin.name/path/myconfig.configuration is a good practice but it doesn't work and I have to put "forder/file.xml".
>
> 3. I have the same problem with the URIs in the diagram properties. I tried to configure "Custom Palette", "Icon" and "Custom Style" diagram properties but I don't know how to put the correct uri to my project files.
>
>    ​
>    When I write "platform:/plugin/my.plugin.name/path/myfile" I get an exception "java.io.IOException: Unable to resolve plug-in"
>
> When I write "folder/file" I get "java.io.FileNotFoundException: folder/file (The system can't found the path)"
>
> (...)
>
> Finally, it worked. I was making a mistake in the name of the plugin. ![Rolling Eyes](https://www.eclipse.org/forums/images/smiley_icons/icon_rolleyes.gif)
>
> Then:
>
> 1. I used the *.xml created by papyrus customizing the palette directly from the model editor and taking the file from the "workspace/.metadata/.plugins/org.eclipse.papyrus.uml.diagram.common" folder. It works fine. I saw some example where they use a .paletteConfiguration file but I still don't know how to create it neither if there is any difference.
>
> 2. If you include the folder in the binary build properties and you type correctly the name of the plugin, it should work. According to the documentation it is the best way to do it.
>
> 3. Finally solved. Only taking care typing the name of the plugin.

### 5. Atualizar código de suporte (menus e automações)