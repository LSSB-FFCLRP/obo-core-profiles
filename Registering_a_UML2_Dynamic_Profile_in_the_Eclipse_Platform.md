---

---



# Registering a UML2 (Dynamic) Profile in the Eclipse Platform

In order to register a UML profile to be used in the Eclipse Platform, 

Name: PROFILE NAME

Namespace: http://SOMETHING/path/MY.profile.uml

Plugin URI: platform:/plugin/PLUGIN_NAME/profile/path/MY.profile.uml

Icon in /PLUGINNAME/resources/img/icon.png



In the Ecore file of the profile, look for the profile package and enter the namespace (http://SOMETHING/path/MY.profile.uml)

In plugin.xml

- Dependencies
  - org.eclipse.emf.ecore
  - org.eclipse.papyrus.uml.extensionpoints


-  Extensions:
  - org.eclipse.emf.ecore.uri_mapping
    - mapping
      - source: http://SOMETHING/path/MY.profile.uml
      - target: platform:/plugin/PLUGIN_NAME/profile/path/MY.profile.uml
    - mapping
      - source: pathmap://UML_PROFILES/<profileNamespace>.profile.uml
      - target: platform:/plugin/PLUGIN_NAME/profile/path/MY.profile.uml
  - org.eclipse.papyrus.uml.extensionpoints.UMLProfile
    - profile
      - name: PROFILE NAME
      - path: http://SOMETHING/path/MY.profile.umll
      - iconpath: resources/img/icon.png