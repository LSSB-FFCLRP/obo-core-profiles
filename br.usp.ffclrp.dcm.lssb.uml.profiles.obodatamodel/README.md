# UML Profile for OBO Ontologies

```
/
|- resources/
|   |- profile/
|   |   |- obodatamodel.profile.uml -> a UML profile with OBO-related stereotypes
|   |--- models/
|   |   |- obofff-builtins.uml -> a UML model (library) with representantions of the builtins objects
```