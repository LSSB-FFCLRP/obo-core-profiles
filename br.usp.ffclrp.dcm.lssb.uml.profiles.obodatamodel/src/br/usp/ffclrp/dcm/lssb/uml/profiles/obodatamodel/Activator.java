package br.usp.ffclrp.dcm.lssb.uml.profiles.obodatamodel;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;


/**
 * The activator class controls the plug-in life cycle
 */

public class Activator implements BundleActivator {

    private static BundleContext context;

    // The plug-in ID
    public static final String PLUGIN_ID = "br.usp.ffclrp.dcm.lssb.uml.profiles.obodatamodel"; //$NON-NLS-1$

    
    
    static BundleContext getContext() {
        return context;
    }

    /*
     * (non-Javadoc)
     * @see org.osgi.framework.BundleActivator#start(org.osgi.framework.BundleContext)
     */
    public void start(BundleContext bundleContext) throws Exception {
        Activator.context = bundleContext;
    }

    /*
     * (non-Javadoc)
     * @see org.osgi.framework.BundleActivator#stop(org.osgi.framework.BundleContext)
     */
    public void stop(BundleContext bundleContext) throws Exception {
        Activator.context = null;
    }

}