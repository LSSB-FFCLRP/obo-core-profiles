/*****************************************************************************
 * Copyright (c) 2016 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *  Benoit Maggi (CEA LIST) benoit.maggi@cea.fr - Initial API and implementation
 *****************************************************************************/
package br.usp.ffclrp.dcm.lssb.uml.profiles.obodatamodel;

import org.eclipse.emf.ecore.resource.ResourceSet;

public class OboDatamodelProfileUtil {
	
	//public static final String PROFILES_PATHMAP = "pathmap://EXTLIBRARY_PROFILE/"; //$NON-NLS-1$	
	
	public static final String PROFILE_PATH = "http://dcm.ffclrp.usp.br/lssb/uml/profiles/obodatamodel.profile.uml"; //$NON-NLS-1$
	
	private OboDatamodelProfileUtil() {
	}
	
	
	/**
	 * Initialize a ResourceSet in order to use the OBO Datamodel Profile.
	 * 
	 * @param resourceSet the resource set to initialize
	 * @return the same resource set initialized, allowing to chain calls
	 */
	public static ResourceSet init(ResourceSet resourceSet) {
	  return resourceSet;
	}
	
	
}
