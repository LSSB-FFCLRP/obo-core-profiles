BFO busca capturar os seguintes tipos de conceitos:

- **entidades** (entity): Termos gerais usados 1) para se referenciar com universais/tipos; ou 2) para referenciar a grupos de entidades que podem instanciar um dado universal mas não correspondem à extensão de qualquer sub-universal desse universal, uma vez que não há nada intrínsico a essas entidades que faz com que elas, e apenas elas, sejam parte do grupo (ex: os animais comprados por João);
    ​
    - **Continuantes (continuant):** entidades que são divididas espacialmente e persistem através do tempo;
        - **Continuantes independentes (independent continuant):**  Continuantes que não dependem de outros continuantes para existir.
            - **Entidades materiais (material entity)**: 
                - **objetos (object):** uma entidade com fronteiras contínuas próprias. Exemplo: um corpo humano.
                - **agregados de objetos (object aggregate):**  conjuntos, grupos  e outras entidades que trocam seus membros, mas mantém a identidade própria.
                - **partes fiat de objetos (fiat object part)** : partes não destacadas de um objeto, que compartilham fronteiras com eles. Exemplo: um braço de um corpo humano;
            - **Entidades imateriais (immaterial entity)**
                - **região espacial (spatial region)**
                    - 0, 1, 2  ou 3 dimensões
                - **sítio (site)**: uma entidade imaterial que é limitada por outras entidades materiais ou imateriais
                - **fronteira fiat de um continuant (continuant fiat boundary)**
                    - 0, 1ou 2  dimensões 
        - **Continuantes especificamente dependentes (specifically dependent continuant)**: Continuantes cuja existência depende de um continuante independente qualquer, não uma região espacial.
            - **Qualidade (quality)**: um continuante especificamente dependente que não depende de processos adicionais para serem realizados. Ex: temperatura de um corpo
                - **Qualidade relacional (relational quality)**: uma qualidade que existem entre dois continuantes em um dado período do tempo. Ex: casamento;
            - **Entidade realizável (realizable entity)**: continuante especificamente dependentes que precisam da execução de um processo adicional para que existam;
                - **Papel (role)**: papel que uma entidade realiza em uma dada interação ou circunstância
                - **disposição (disposition)**:  entidade realizável que uma entidade material qualquer possui, que a realização ocorre quando/porquê esse possuidor está em uma dada circunstância física. Ex: a parece celular é *disposta para filtrar* elementos químicos.
                    - **função (function)**:  uma disposição que, de alguma forma evolucionária ou de projeto, definiu a constituição física do seu possuidor. Por exemplo, martelar pregos é função possúida por um martelo, e os martelos evoluíram/são criados para que seja possível martelar pregos melhor.
        - **Continuantes genericamente dependentes (generically dependent continuant)**: Um continuante que depende de forma geral em uma ou mais entidades. Exemplo: o arquivo PDF em um laptop e a cópia desse arquivo em outro laptop oa  a mesma entidade/informação, sendo uma entidade cuja existência é dependente de diferentes laptops; um banco de dados (cujos registros são padrões instanciados como instâncias de qualidades no disco, ou information content entity da IAO).
            ​
            ​

    - **Ocorrentes (Occurents):** Entidades que são divididas temporalmente e existem em uma dada região espaço-temporal
        - **processos (process):** Um ocorrente que tem partes próprias e depende de uma dada entidade material;
            - **histórias (history):** um processo que é a soma da totalidade dos processos que ocorrem em uma dada região espacial.
            - **perfil de processos (process profile):** uma entidade que representa um conjunto de características de outros processos
        - **fronteiras de processos (process boundary):** parte temporal de um processo que não possui partes temporais próprias.
        - **região temporal (temporal region):** uma entidade ocorrente que é parte do tempo e é definida segundo algum quadro de referência desse tempo 
            - 0 ou 1 dimensional
        - **região espaço temporal (spatiotemporal region):** um ocorrente que é parte do espaço-tempo.