# Papyrus UML - Extending existing profiles

## Problem

Let's say that we have two profile definitions in the workspace, *Pa* and *Pb*. Some stereotype *Sb* present in *Pb* must extend a stereotype *Sa* in *Pa*. How to do this in Papyrus?

## Question points

- When importing using the `Load Resource` menu item, the profile *Pa* is registered using a `platform:resource/` URI. This kind of URI references resources in the workspace. Hence, when the profile is registered in the platform  by a plugin (and thus it doesn't exists in the workspace itself), the reference points nowhere.  Resources provided by an Eclipse plugin must  be referenced by a `platform:plugin/` URI ou some URI mapping;

 ## Current answer

1. Export *Pa* in a dedicated Eclipse plugin pPa (recomendation: export it to an Update Site);

2. Install the plugin *pPa* in the Eclipse instance used to develop *Pb*.

   > This shall register the profile in the Eclipse platform.

3. Open the *Pb* profile diagram;

4. In the `Model Explorer`view of Papyrus, right click the profile *Pb* (folder-like icon) and choose `Import > Import Registered Profile`. Then, select the profile *Pa* in the list and choose it  to be applied;

5. Finally, open the imported profile in the `Model Explorer` view and drag the desired stereotype *Sa* to the diagram.

   > The profile will be loaded using the definitive URI.