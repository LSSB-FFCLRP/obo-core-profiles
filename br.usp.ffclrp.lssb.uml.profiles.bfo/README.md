# BFO Classes Minimal Profile

Based on the [BFO-Classes-Minimal ontology](https://raw.githubusercontent.com/oborel/obo-relations/master/bfo-classes-minimal.owl), imported by the [Relations Ontology Core](https://github.com/oborel/obo-relations/wiki/ROCore).

## Creation process: 

1. Created a Papyrus profile diagram (Root element name: `BFOClassesMinimal`);
2. In the `Profile`tab of the Properties View, added the profile for OWL 1.1 by its pathmap URL (`pathmap://ODM_1_1_PROFILES/OWLProfile1.profile.uml`);
3. In the Model Explores view, dragged the OWL profile stereotypes into the diagram;
4. Created new stereotypes, extending a stereotype with other when a `rdfs:subClassOf`relation exists in the given ontology;
5. All stereotypes have their name based in the OWL class name in camel case with the first word in lowercase. 

URI: http://lssb.ffclrp.usp.br/uml/profiles/bfo-minimal.profile.uml


## Rationale for creation of stereotypes

In OWL, classes are a abstraction mechanism fro grouping resources with similar characteristics. OWL Classes are described by `class expressions`, which can be combined in `class axioms`. Class expressions can describe a class through a name or by specifying class extensions (i.e., individuals) of an unnamed anonymous class. There are six different types of classes expressions in OWL [@odm:omg:1.1]:

1. **Class identifiers:** Describe a class through a name (syntacticly, a IRI);

   ```
   Declaration( Class( Person ) )
   ```

   ​

2. **Exhaustive enumeration of individuals:** As the name says, defines a class Example:

   ```
   EquivalentClasses(
      :MyBirthdayGuests
      ObjectOneOf( :Bill :John :Mary)
    )
   ```

3. **Property Restrictions:** Describe special kinds on class expressions that may be anonymous and consists of all the individuals that satisfy the stated restriction. Such restriction can be a *value constraint*  or a *cardinality constraint*. A *value constraint* constrains the range of the property when applied to a particular class expression. A *cardinality constraint* constrain the number of values a property can have in the context of the class expression it is applied to. In this sense, a cardinality constraint is similar to a UML Multiplicity element, allowing to set a number to the minimum, maximum or exact cardinality. Examples: 

   1. A *Parent* is a individual that have some *person* in its property *hasChild*.: 

     ```
      EquivalentClasses(
        :Parent 
        ObjectSomeValuesFrom( :hasChild :Person )
      )
     ```
   2. John has exactly 5 values in its property  *hasChild* (John have 5 children):
     ```
      ClassAssertion(
        ObjectExactCardinality( 5 :hasChild ) 
        :John
      )
     ```

4. **Intersections of class expressions:** Describe classes as all the individuals in the intersection of a number of other classes. Example: A *Mother* is a individual that is a *Woman* and also is a *Parent*;
   ```
      EquivalentClasses(
        :Mother 
        ObjectIntersectionOf( :Woman :Parent )
      )
   ```


5. **Unions of class expressions:** Describe classes as all the individuals in the union of a number of other classes. Example: A *Parent* is a individual that is a *Mother* or a *Father*;

   ```
    EquivalentClasses(
      :Parent 
      ObjectUnionOf( :Mother :Father )
    ) 
   ```

   ​

6. **Complements of class expressions:** Defines a class as the individuals that are in the complement of another class expression. Example: A *ChildlessPerson* is a individual present in the intersection of all *Persons* and that is not a *Parent*;

   ```
    EquivalentClasses(
      :ChildlessPerson 
      ObjectIntersectionOf(
        :Person 
        ObjectComplementOf( :Parent )
      )
    ) 
   ```

   ​



All classes in the  BFO minimum profile are defined by class identifiers.  

We want to allow users to model that UML classes can be stereotypes by  BFO minimum profile classes.

However, we can't define those new stereotypes is a specialization of one of the stereotypes  that represents class expressions. This would limit their application. For example, if we define that only Class Identifier expression can be a Continuant, we cannot represent Continuants as anonymous equivalent classes0