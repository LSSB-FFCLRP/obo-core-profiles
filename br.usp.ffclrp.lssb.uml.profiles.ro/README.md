# Relations Ontology Core UML Profile

Based on the  [Relations Ontology Core](https://github.com/oborel/obo-relations/wiki/ROCore).

## Rationale

All stereotypes are applied to Associations.